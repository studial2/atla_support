<<INSTRUCTIONS
The script creates packages on this machine, move it under /tmp/ partition and outputs size and locations of the created packages.
Before start please grant 755 permissions to the script file and folder where it is stored. Change directory to root: `cd /`
The script should be started from / with full path to the script, example: `/home.ec2/ec2-user/create_packages.sh sw all`
Also please be aware about parameters:
    First parameter: define application - Software or Servicedesk, possible values: 'sw' or 'sd'.
    The next parameters are about list of packages, possible values:
        'all' - all packages
        if you do not need all packages, please list only those you need:
        'home' - atlassian-home package
        'install' - atlassian-install package
        'plugins' - atlassian-plugins package
        'shared' - atlassian-shared package
    The order of the package list does not matter.

Examples of the correct calling script with parameters:
    [path_to_script]/create_packages.sh sw all
    - will create all Software packages

    [path_to_script]/create_packages.sh sd install shared
    - will create install and shared ServiceDesk packages

    [path_to_script]/create_packages.sh sd shared home plugins
    - will create shared, home and plugins ServiceDesk packages

INSTRUCTIONS

#Checking parameters
if [ $# -lt 2 ]; then
    echo "Please specify all parameters and try again. See instructions inside the script."
    echo "Execution is stopped."
    exit 1
elif ! echo $1 | egrep -E "sw|sd"; then
    echo "Please specify sw or sd and try again. See instructions inside the script."
    echo "Execution is stopped."
    exit 1
fi

#set package list
if [ $2 == 'all' ]; then
   packageset='home install plugins shared'
else
   packageset=$*
fi

#set application
if [ $1 == 'sw' ]; then
    app='software'
elif [ $1 == 'sd' ]; then
    app='servicedesk'
else
    echo "Possible values for the first parameter are: `sw` or `sd`. See instructions inside the script, think about it and try again."
    echo "Execution is stopped."
    exit 1
fi

#Exclude list file cretaion
touch `dirname "$0"`/temp_exclude_list
echo "application-data/export/*
application-data/jira/.jira-home.lock
application-data/jira/caches/*
application-data/jira/log/*
application-data/jira/plugins/.bundled-plugins
application-data/jira/plugins/.osgi-plugins
application-data/jira/temp/*
application-data/jira/tmp/*
jira/logs/*
jira/work/catalina.pid
shared/analytics-logs/*
shared/caches/*
shared/data/*
shared/eazybi.toml
shared/export/*
shared/import/*
shared/logs
shared/node-status/*
shared/plugins/.bundled-plugins
shared/plugins/.osgi-plugins" > `dirname "$0"`/temp_exclude_list

#Packages creation
#temporary folder creation
tmpdirpath="/tmp/tmp_packs_`date +%Y-%m-%d`"
mkdir $tmpdirpath
mkdir $tmpdirpath/$app
tmpdirpath="$tmpdirpath/$app"

#home package
if echo $packageset | grep -q "home"; then
    cd /var/atlassian/
    tar -czvf atlassian-home.tar.gz --exclude-from=`dirname "$0"`/temp_exclude_list application-data/
    mv atlassian-home.tar.gz $tmpdirpath
    atlassianHomePath="`du -h --max-depth=0 $tmpdirpath/atlassian-home.tar.gz`\n"
fi

#install package
if echo $packageset | grep -q "install"; then
    cd /opt/atlassian/
    tar -czvf atlassian-install.tar.gz --exclude-from=`dirname "$0"`/temp_exclude_list jira/
    mv atlassian-install.tar.gz $tmpdirpath
    atlassianInstallPath="`du -h --max-depth=0 $tmpdirpath/atlassian-install.tar.gz`\n"
fi

#plugins package
if echo $packageset | grep -q "plugins"; then
    cd /media/atl/jira/shared/$app/shared/plugins/
    touch REMOVE_TO_UNPACK_AGAIN.txt
    tar -czvf atlassian-plugins.tar.gz --exclude-from=`dirname "$0"`/temp_exclude_list installed-plugins/ REMOVE_TO_UNPACK_AGAIN.txt
    mv atlassian-plugins.tar.gz $tmpdirpath
    atlassianPluginsPath="`du -h --max-depth=0 $tmpdirpath/atlassian-plugins.tar.gz`\n"
fi

#shared package
if echo $packageset | grep -q "shared"; then
    touch /media/atl/jira/shared/$app/shared/REMOVE_TO_UNPACK_AGAIN.txt
    cd /media/atl/jira/shared/$app/
    tar -czvf atlassian-shared.tar.gz --exclude-from=`dirname "$0"`/temp_exclude_list shared/
    mv atlassian-shared.tar.gz $tmpdirpath
    atlassiaSharedPath="`du -h --max-depth=0 $tmpdirpath/atlassian-shared.tar.gz`\n"
fi


#delete exclude list file
rm -rf `dirname "$0"`/temp_exclude_list

#Info output
echo -e "-----------------------------------
The created packages are located:\n"
echo -e $atlassianHomePath
echo -e $atlassianInstallPath
echo -e $atlassianPluginsPath
echo -e $atlassiaSharedPath
echo -e "Please delete the created packages after downloading
-----------------------------------"
